package com.dms.filters;

public enum UriPaths {
    DEVICE("/device");

    private String path;

    UriPaths(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
