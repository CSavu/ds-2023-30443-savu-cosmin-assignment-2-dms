package com.dms.rabbitmq.consumers;

import com.dms.services.DeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class UserUpdateConsumer {
    @Autowired
    private DeviceService deviceService;

    @RabbitListener(queues = {"${dms.rabbitmq.user.delete.queue}"})
    public void receiveUserDeletionMessage(String userId) {
        log.info("User deletion message received for userId={}", userId);
        deviceService.deleteAllUserDeviceMappings(Integer.parseInt(userId));
    }

}
