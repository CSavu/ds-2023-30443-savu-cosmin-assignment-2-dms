package com.dms.configs;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfig {

    @Value("${dms.rabbitmq.caching.connection.factory.hostname}")
    private String hostname;

    @Value("${dms.rabbitmq.device.exchange}")
    private String deviceExchange;
    @Value("${dms.rabbitmq.device.delete.queue}")
    private String deviceDeleteQueue;
    @Value("${dms.rabbitmq.device.delete.routing_key}")
    private String deviceDeleteRoutingKey;

    @Bean
    public CachingConnectionFactory connectionFactory() {
        return new CachingConnectionFactory(hostname);
    }

    @Bean
    public RabbitAdmin amqpAdmin() {
        return new RabbitAdmin(connectionFactory());
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        return new RabbitTemplate(connectionFactory());
    }

    @Bean
    public Binding binding() {
        return BindingBuilder.bind(deviceDeleteQueue())
                .to(deviceExchangeTopic())
                .with(deviceDeleteRoutingKey);
    }

    @Bean
    public TopicExchange deviceExchangeTopic() {
        return new TopicExchange(deviceExchange);
    }

    @Bean
    public Queue deviceDeleteQueue() {
        return new Queue(deviceDeleteQueue);
    }

}
