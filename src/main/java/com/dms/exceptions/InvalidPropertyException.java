package com.dms.exceptions;

import lombok.Getter;

@Getter
public class InvalidPropertyException extends RuntimeException {
    public InvalidPropertyException() {
        super("Invalid property of object!");
    }
}
